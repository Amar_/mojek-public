const { Router } = require("express");
const router = Router();
const User = require("../models/User");
const crypto = require("../controller/crypto");
const userAuth = require("../middlewares/user");
const userLoginInitAuth = require("../middlewares/user-login-init");
const userController = require("../controller/user");

router.post("/create", async (req, res) => {
    let { email, phone, name, password } = req.body;
    if (!email) {
        return res.status(500).send({ error: true, message: "Invalid Email ID" })
    }
    if (!phone) {
        return res.status(500).send({ error: true, message: "Invalid Phone Number" })
    }
    const isUser = await User.findOne({ email });
    if (isUser) {
        return res.status(500).send({ error: true, message: "Account already exist. Please login." })
    }
    phone = crypto.encrypt(phone);
    // name = crypto.encrypt(name);

    let userData = {
        email,
        phone,
        name,
        password
    }

    const user = new User(userData);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.send({ token })
    } catch (error) {
        res.status(500).send(error)
    }
})

router.post("/add-passcode", userAuth, async (req, res) => {
    let { passCode } = req.body;
    if (!passCode) {
        return res.status(500).send({ error: true, message: "Passcode cannot be empty" })
    }
    if (passCode.length != 7) {
        return res.status(500).send({ error: true, message: "Passcode must be 7 digit" })
    }
    passCode = await crypto.encrypt(passCode);

    req.user.passcode = passCode;
    try {
        await req.user.save();
        res.send({ status: true });
    } catch (error) {
        console.log(error);
        res.status(500).send(error);
    }

})

router.post("/login/init", async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await User.findByEmailCredentials(email, password)
        if (!user) {
            throw new Error({ error: true, message: "Unable to login" });
        }
        const loginToken = await user.generateLoginInitToken();
        res.send({ loginToken });
    } catch (error) {
        res.status(401).send(error)
    }
})

router.post("/login", userLoginInitAuth, async (req, res) => {
    const { passCode } = req.body;
    try {
        let passcode = req.user.passcode;
        passcode = crypto.decrypt(passcode);
        if (passcode == passCode) {
            const token = await req.user.generateAuthToken();
            req.user.loginTokens = req.user.loginTokens.filter((logintoken) => {
                return logintoken.token !== req.loginToken;
            });
            await req.user.save();
            res.send({ token });
        } else {
            throw new Error()
        }
    } catch (error) {
        res.status(401).send(error)
    }
})

router.get("/integrations", userAuth, (req, res) => {
    const userID = req.user._id;
    userController.getIntegrations(userID).then(integrations => {
        res.send(integrations)
    }).catch(error => {
        res.status(500).send(error);
    })
})

router.post("/logout", userAuth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
        });
        await req.user.save();
        res.send();
    } catch (error) {
        console.log(error);
        res.status(500).send();
    }
})

module.exports = router