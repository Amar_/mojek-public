const { Router } = require("express");
const router = Router();
const userAuth = require("../middlewares/user");
const mongoose = require('mongoose');
const moment = require("moment");

const providerController = require("../controller/providers");
const integrationController = require("../controller/integration")
const userController = require("../controller/user");
const crypto = require("../controller/crypto");

router.get("/providers", userAuth, (req, res) => {
    const userID = req.user._id;
    providerController.getProviders(userID).then(async providers => {
        const integrations = await userController.getIntegrations(userID);
        for (let i = 0; i < integrations.length; i++) {
            const integration = integrations[i];
            const integrationIndex = providers.findIndex((provider) => {
                return provider._id + "" == integration.provider.id + "";
            })
            if (integrationIndex > -1) {
                providers[integrationIndex].integrationStatus = true;
                providers[integrationIndex].accountID = integration.accountID;
            }
        }
        res.send(providers);
    }).catch(error => {
        console.log(error);
        res.status(500).send(error);
    })
})

router.post("/integrate", userAuth, async (req, res) => {
    const { providerID } = req.body;
    const userEmail = req.user.email;
    try {
        const account = await providerController.getProvider(providerID);
        const { provider, name: providerName } = account;
        let connectURL;

        // Based on the provider -> return connect URL

    } catch (error) {
        res.status(400).send({ error: true, message: "Couldn't get the connect URL" });
    }
})

router.post("/integrate/add-account", userAuth, async (req, res) => {
    let { providerID, accountID, apiKey, apiSecret, requestToken } = req.body;
    const userEmail = req.user.email;
    const userID = req.user._id;

    try {
        if (!providerID) {
            return res.status(400).send({ error: true, message: "Invalid Provider ID" })
        }
        const providerData = await providerController.getProvider(providerID);

        // Integrate the account based on the provider

        res.send({ status: true, message: `Account successfully integrated with ${providerData.display_name}` });
    } catch (error) {
        console.log(error);
        res.status(400).send({ error: true, message: error.message ? error.message : "Unknown error occured!" });
    }
})

router.post("/remove-account", userAuth, async (req, res) => {
    let userEmail = req.user.email;
    let userID = req.user._id;
    let providerID = req.body.providerID;
    if (!providerID) {
        return res.status(400).send({ error: true, message: "Provider ID is missing" });
    }
    try {
        // Get Account ID from the Integration Data
        const accountData = await integrationController.getAccoundIDFromProvider(userID, providerID);
        const accountID = accountData && accountData.accountID;
        if (!accountID) {
            return res.status(400).send({ error: true, message: "Invalid Account Selected!" })
        }

        // Check who the provider is
        const account = await providerController.getProvider(providerID);
        const { provider, display_name: providerName } = account;


        await integrationController.removeintegration(accountID, false, userID);

        res.send({ status: true, message: `${providerName} has been successfully removed from our server.` })
    } catch (error) {
        console.log(error);
        res.status(400).send({ error: true, message: `Error while removing the account` });
    }
})

router.post("/get-transactions", userAuth, async (req, res) => {
    let userEmail = req.user.email;
    let userID = req.user._id;
    let providerID = req.body.providerID;

    try {
        // Get Account ID from the Integration Data
        const accountData = await integrationController.getAccoundIDFromProvider(userID, providerID);
        const accountID = accountData && accountData.accountID;
        if (!accountID) {
            return res.status(400).send({ error: true, message: "Invalid Account Selected!" })
        }

        // Check who the provider is
        const account = await providerController.getProvider(providerID);
        const { provider } = account;

        // Get Transactions from the providers

    } catch (error) {
        console.log(error);
        res.status(400).send(error)
    }
})

router.post("/get-balances", userAuth, async (req, res) => {
    let userEmail = req.user.email;
    let userID = req.user._id;
    let providerID = req.body.providerID;

    try {
        // Get Account ID from the Integration Data
        const accountData = await integrationController.getAccoundIDFromProvider(userID, providerID);
        const accountID = accountData && accountData.accountID;
        if (!accountID) {
            return res.status(400).send({ error: true, message: "Invalid Account Selected!" })
        }

        // Check who the provider is
        const account = await providerController.getProvider(providerID);
        const { provider } = account;



    } catch (error) {
        res.status(400).send({ error: true, message: "Unable to fetch balance at the moment. Please try again later." });
    }
})

router.get("/portfolio", userAuth, async (req, res) => {
    try {
        const userID = req.user._id;
        const userEmail = req.user.email;

        const lastBalanceFetched = req.user.lastBalanceFetched;
        if (lastBalanceFetched) {
            let timeNow = new Date();
            const duration = moment(timeNow).diff(moment(lastBalanceFetched), 'minutes');
            if (duration > 5) {
                await userController.updateWallets(userID, userEmail);
                req.user.lastBalanceFetched = new Date();
                await req.user.save();
            }
        } else {
            await userController.updateWallets(userID, userEmail);
            req.user.lastBalanceFetched = new Date();
            await req.user.save();
        }
        const portfolio = await integrationController.getPortFolio(userID)
        res.send(portfolio)

    } catch (error) {
        res.status(500).send(error)
    }
})

router.post("/update-wallets", userAuth, async (req, res) => {
    const userID = req.user._id;
    const userEmail = req.user.email;

    try {
        await userController.updateWallets(userID, userEmail);
        req.user.lastBalanceFetched = new Date();
        await req.user.save();
        res.send({ status: true })
    } catch (error) {
        console.log(error);
        res.status(400).send({ error: true, message: "Unknown error occured!" })
    }
})

module.exports = router