const mongoose = require("mongoose");
var Float = require('mongoose-float').loadType(mongoose);

var balancesSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    accountID: {
        type: String,
        required: true
    },
    provider: {
        name: {
            type: String
        },
        display_name: {
            type: String
        },
        logo: {
            type: String
        },
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "providers",
            required: true
        }
    },
    ticker: {
        type: String
    },
    provider_ticker: {
        type: String
    },
    name: {
        type: String
    },
    amount: {
        type: String
    },
    type: {
        type: String
    },
    decimals: {
        type: Number
    },
    fiat_value: {
        type: Float,
    },
    fiat_ticker: {
        type: String
    },
    portfolioCurrency: {
        type: String
    },
    logo: {
        type: String
    },
    balanceUpdatedAt: {
        type: Number
    },
    exchange: {
        type: String
    },
    profitLoss: {
        type: Float
    },
    last_price: {
        type: Float
    },
    currentValue: {
        type: Float,
    },
    balanceDate: {
        type: String
    },
    wallet: {
        type: String
    }
});

balancesSchema.set("timestamps", true);

const Balances = mongoose.model("balances", balancesSchema);

module.exports = Balances;